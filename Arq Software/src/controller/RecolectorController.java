package controller;

public class RecolectorController {
	private static final int ID_CIUDAD = 1;
	private static final int ID_DISPOSITIVO = 1;
	private static final String MESSAGE_DEFAULT = "DAT1%";
	private static final String MESSAGE_LED_ON = "LED13ON%";
	private static final String MESSAGE_LED_OFF = "LED13OFF%";
	private static final int INTERVAL_LED = 8000;
	//private static final int INTERVAL_DATA = 10000;
	private static final int INTERVAL_DATA = 180000;

	public RecolectorController() {
		super();
	}

	public void init() {
		SerialEventController main = new SerialEventController(ID_CIUDAD, ID_DISPOSITIVO);
		main.initialize();
		Thread t = new Thread() {
			public void run() {
				while (true)
					try {
						main.sendData(MESSAGE_LED_ON);
						Thread.sleep(INTERVAL_LED);
						main.sendData(MESSAGE_LED_OFF);
						main.sendData(MESSAGE_DEFAULT);
						Thread.sleep(INTERVAL_DATA - INTERVAL_LED);
					} catch (InterruptedException ie) {
					}
			}
		};
		t.start();
		System.out.println("Started");
	}

}
