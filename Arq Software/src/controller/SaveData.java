package controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Ciudad;
import model.Dispositivo;
import model.Lecturastemperatura;

public class SaveData {

	private static EntityManagerFactory emfactory;
	private EntityManager entitymanager;

	public SaveData() {
		if (emfactory == null)
			emfactory = Persistence.createEntityManagerFactory("Arq Software");
		entitymanager = emfactory.createEntityManager();
	}

	public boolean saveTemperature(float data,int idCiudad, int idDispositivo) {
		Dispositivo dispositivo = getDispositivo(idDispositivo);
		Ciudad ciudad = getCiudad(idCiudad);
		Date fechaConsulta = new Date();
		Lecturastemperatura lectura = new Lecturastemperatura();
		lectura.setCiudad(ciudad);
		lectura.setFechacaptura(new Timestamp(fechaConsulta.getTime()));
		lectura.setDispositivo(dispositivo);
		lectura.setValortemperatura(new BigDecimal(data));

		if (!entitymanager.getTransaction().isActive())
			entitymanager.getTransaction().begin();
		entitymanager.persist(lectura);
		entitymanager.getTransaction().commit();
		return true;
	}

	@SuppressWarnings("unchecked")
	public Ciudad getCiudad(int ciudad) {
		if (!entitymanager.getTransaction().isActive())
			entitymanager.getTransaction().begin();
		Query query = entitymanager.createNamedQuery("Ciudad.findById");
		query.setParameter("ciudad", ciudad);
		query.setMaxResults(1);
		List<Ciudad> ciudades = (List<Ciudad>) query.getResultList();
		if (ciudades.isEmpty())
			return null;
		return ciudades.get(0);
	}

	@SuppressWarnings("unchecked")
	public Dispositivo getDispositivo(int disp) {
		if (!entitymanager.getTransaction().isActive())
			entitymanager.getTransaction().begin();
		Query query = entitymanager.createNamedQuery("Dispositivo.findById");
		query.setParameter("disp", disp);
		query.setMaxResults(1);
		List<Dispositivo> dispositivos = (List<Dispositivo>) query.getResultList();
		if (dispositivos.isEmpty())
			return null;
		return dispositivos.get(0);
	}
}
