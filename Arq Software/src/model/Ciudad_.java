package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-13T20:31:30.157-0500")
@StaticMetamodel(Ciudad.class)
public class Ciudad_ {
	public static volatile SingularAttribute<Ciudad, Long> idciudad;
	public static volatile SingularAttribute<Ciudad, Integer> estado;
	public static volatile SingularAttribute<Ciudad, String> nombreciudad;
	public static volatile SingularAttribute<Ciudad, Departamento> departamento;
	public static volatile ListAttribute<Ciudad, Lecturastemperatura> lecturastemperaturas;
}
