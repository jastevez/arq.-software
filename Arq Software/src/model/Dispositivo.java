package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the dispositivo database table.
 * 
 */
@Entity
@Table(name="dispositivo")
@NamedQueries({
@NamedQuery(name="Dispositivo.findAll", query="SELECT d FROM Dispositivo d"),
@NamedQuery(name="Dispositivo.findById", query="SELECT d FROM Dispositivo d where d.iddispositivo = :disp")})
public class Dispositivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, precision=131089)
	private long iddispositivo;

	@Column(nullable=false)
	private Integer estado = 1;

	@Column(nullable=false, length=60)
	private String nombredispositivo;

	//bi-directional many-to-one association to Lecturastemperatura
	@OneToMany(mappedBy="dispositivo")
	private List<Lecturastemperatura> lecturastemperaturas;

	public Dispositivo() {
	}

	public long getIddispositivo() {
		return this.iddispositivo;
	}

	public void setIddispositivo(long iddispositivo) {
		this.iddispositivo = iddispositivo;
	}

	public Integer getEstado() {
		return this.estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getNombredispositivo() {
		return this.nombredispositivo;
	}

	public void setNombredispositivo(String nombredispositivo) {
		this.nombredispositivo = nombredispositivo;
	}

	public List<Lecturastemperatura> getLecturastemperaturas() {
		return this.lecturastemperaturas;
	}

	public void setLecturastemperaturas(List<Lecturastemperatura> lecturastemperaturas) {
		this.lecturastemperaturas = lecturastemperaturas;
	}

	public Lecturastemperatura addLecturastemperatura(Lecturastemperatura lecturastemperatura) {
		getLecturastemperaturas().add(lecturastemperatura);
		lecturastemperatura.setDispositivo(this);

		return lecturastemperatura;
	}

	public Lecturastemperatura removeLecturastemperatura(Lecturastemperatura lecturastemperatura) {
		getLecturastemperaturas().remove(lecturastemperatura);
		lecturastemperatura.setDispositivo(null);

		return lecturastemperatura;
	}

}