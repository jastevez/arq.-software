package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ciudad database table.
 * 
 */
@Entity
@Table(name="ciudad")
@NamedQueries({ 
@NamedQuery(name="Ciudad.findAll", query="SELECT c FROM Ciudad c"),
@NamedQuery(name="Ciudad.findById", query="SELECT c FROM Ciudad c where c.idciudad = :ciudad"),
})
public class Ciudad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, precision=131089)
	private long idciudad;

	@Column(nullable=false)
	private Integer estado = 1;

	@Column(nullable=false, length=60)
	private String nombreciudad;

	//bi-directional many-to-one association to Departamento
	@ManyToOne
	@JoinColumn(name="iddepartamento", nullable=false)
	private Departamento departamento;

	//bi-directional many-to-one association to Lecturastemperatura
	@OneToMany(mappedBy="ciudad")
	private List<Lecturastemperatura> lecturastemperaturas;

	public Ciudad() {
	}

	public long getIdciudad() {
		return this.idciudad;
	}

	public void setIdciudad(long idciudad) {
		this.idciudad = idciudad;
	}

	public Integer getEstado() {
		return this.estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getNombreciudad() {
		return this.nombreciudad;
	}

	public void setNombreciudad(String nombreciudad) {
		this.nombreciudad = nombreciudad;
	}

	public Departamento getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public List<Lecturastemperatura> getLecturastemperaturas() {
		return this.lecturastemperaturas;
	}

	public void setLecturastemperaturas(List<Lecturastemperatura> lecturastemperaturas) {
		this.lecturastemperaturas = lecturastemperaturas;
	}

	public Lecturastemperatura addLecturastemperatura(Lecturastemperatura lecturastemperatura) {
		getLecturastemperaturas().add(lecturastemperatura);
		lecturastemperatura.setCiudad(this);

		return lecturastemperatura;
	}

	public Lecturastemperatura removeLecturastemperatura(Lecturastemperatura lecturastemperatura) {
		getLecturastemperaturas().remove(lecturastemperatura);
		lecturastemperatura.setCiudad(null);

		return lecturastemperatura;
	}

}