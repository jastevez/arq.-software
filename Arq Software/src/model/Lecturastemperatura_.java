package model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-13T20:31:30.200-0500")
@StaticMetamodel(Lecturastemperatura.class)
public class Lecturastemperatura_ {
	public static volatile SingularAttribute<Lecturastemperatura, Integer> idlectura;
	public static volatile SingularAttribute<Lecturastemperatura, Integer> estado;
	public static volatile SingularAttribute<Lecturastemperatura, Timestamp> fechacaptura;
	public static volatile SingularAttribute<Lecturastemperatura, BigDecimal> valortemperatura;
	public static volatile SingularAttribute<Lecturastemperatura, Ciudad> ciudad;
	public static volatile SingularAttribute<Lecturastemperatura, Dispositivo> dispositivo;
}
