package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the departamento database table.
 * 
 */
@Entity
@Table(name="departamento")
@NamedQueries({ 
@NamedQuery(name="Departamento.findAll", query="SELECT d FROM Departamento d"),
@NamedQuery(name="Departamento.findById", query="SELECT d FROM Departamento d where d.iddepartamento = :dep")})
public class Departamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, precision=131089)
	private long iddepartamento;

	@Column(nullable=false)
	private Integer estado = 1;

	@Column(nullable=false, length=60)
	private String nombredepartamento;

	//bi-directional many-to-one association to Ciudad
	@OneToMany(mappedBy="departamento")
	private List<Ciudad> ciudads;

	public Departamento() {
	}

	public long getIddepartamento() {
		return this.iddepartamento;
	}

	public void setIddepartamento(long iddepartamento) {
		this.iddepartamento = iddepartamento;
	}

	public Integer getEstado() {
		return this.estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getNombredepartamento() {
		return this.nombredepartamento;
	}

	public void setNombredepartamento(String nombredepartamento) {
		this.nombredepartamento = nombredepartamento;
	}

	public List<Ciudad> getCiudads() {
		return this.ciudads;
	}

	public void setCiudads(List<Ciudad> ciudads) {
		this.ciudads = ciudads;
	}

	public Ciudad addCiudad(Ciudad ciudad) {
		getCiudads().add(ciudad);
		ciudad.setDepartamento(this);

		return ciudad;
	}

	public Ciudad removeCiudad(Ciudad ciudad) {
		getCiudads().remove(ciudad);
		ciudad.setDepartamento(null);

		return ciudad;
	}

}