package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the lecturastemperatura database table.
 * 
 */
@Entity
@Table(name="lecturastemperatura")
@SequenceGenerator(name = "lectura_seq", initialValue = 1, allocationSize = 1)
@NamedQuery(name="Lecturastemperatura.findAll", query="SELECT l FROM Lecturastemperatura l")
public class Lecturastemperatura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lectura_seq")
	@Column(unique=true, nullable=false)
	private Integer idlectura;

	@Column(nullable=false)
	private Integer estado = 1;

	@Column(nullable=false)
	private Timestamp fechacaptura;

	@Column(nullable=false, precision=7)
	private BigDecimal valortemperatura;

	//bi-directional many-to-one association to Ciudad
	@ManyToOne
	@JoinColumn(name="idciudad", nullable=false,columnDefinition="default 1")
	private Ciudad ciudad;

	//bi-directional many-to-one association to Dispositivo
	@ManyToOne
	@JoinColumn(name="iddispositivo", nullable=false)
	private Dispositivo dispositivo;

	public Lecturastemperatura() {
	}

	public Integer getIdlectura() {
		return this.idlectura;
	}

	public void setIdlectura(Integer idlectura) {
		this.idlectura = idlectura;
	}

	public Integer getEstado() {
		return this.estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Timestamp getFechacaptura() {
		return this.fechacaptura;
	}

	public void setFechacaptura(Timestamp fechacaptura) {
		this.fechacaptura = fechacaptura;
	}

	public BigDecimal getValortemperatura() {
		return this.valortemperatura;
	}

	public void setValortemperatura(BigDecimal valortemperatura) {
		this.valortemperatura = valortemperatura;
	}

	public Ciudad getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public Dispositivo getDispositivo() {
		return this.dispositivo;
	}

	public void setDispositivo(Dispositivo dispositivo) {
		this.dispositivo = dispositivo;
	}

}