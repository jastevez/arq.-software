package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-13T20:31:30.191-0500")
@StaticMetamodel(Departamento.class)
public class Departamento_ {
	public static volatile SingularAttribute<Departamento, Long> iddepartamento;
	public static volatile SingularAttribute<Departamento, Integer> estado;
	public static volatile SingularAttribute<Departamento, String> nombredepartamento;
	public static volatile ListAttribute<Departamento, Ciudad> ciudads;
}
