package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-13T20:31:30.195-0500")
@StaticMetamodel(Dispositivo.class)
public class Dispositivo_ {
	public static volatile SingularAttribute<Dispositivo, Long> iddispositivo;
	public static volatile SingularAttribute<Dispositivo, Integer> estado;
	public static volatile SingularAttribute<Dispositivo, String> nombredispositivo;
	public static volatile ListAttribute<Dispositivo, Lecturastemperatura> lecturastemperaturas;
}
